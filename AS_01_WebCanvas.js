var canvas = document.getElementById('mycanvas');
var brushSize = document.getElementById('brushSize');
var ctx = canvas.getContext("2d");
var imgLoader = document.getElementById('uploadImg');
var colorSelector = document.getElementById('colorPickSelector');
// var btn = document.getElementsByClassName('btn');
// var prePress;
// var btnC = Array.prototype.filter.call(btn, function(btn){
//     btn.addEventListener('click', function() {
//         btn.style.opacity = 0.8;
//     })
// });

var picArray = new Array();
var picStep = -1;
var picSav = 0;
var canvasPic = new Image();
var tool = 'brush';
var color = 'black';
var size = 3;
var font = "Arial";
var fontSize = 8;
var startX = -1;
var startY = -1;
var endX = -1;
var endY = -1;
var hue = 0;
var lastPoint;



canvas.addEventListener("mousedown", (event) => {
    startX = event.offsetX;
    startY = event.offsetY;

    ctx.fillStyle = colorSelector.style.color;
    ctx.strokeStyle = colorSelector.style.color;
    ctx.lineWidth = size;
    switch(tool){
        case 'rainbow':
            hue = 0;
        case 'brush':
            lastPoint = {x: event.offsetX, y: event.offsetY};
            break;
        case 'eraser':
            ctx.beginPath();
            ctx.fillStyle = 'white';
            ctx.strokeStyle = 'white';
            ctx.lineWidth = size*7;
            ctx.moveTo(startX, startY);
            break;
        case 'circle':
            ctx.beginPath();
            break;
        case 'triangle':
            ctx.beginPath();
            break;
        case 'text':
            typeText();
            break;
        default:
            ctx.fillStyle =  colorSelector.style.color;
            ctx.strokeStyle =  colorSelector.style.color;
            ctx.lineWidth = size*3;
            break;
    }
    canvas.addEventListener("mousemove", draw);
});

canvas.addEventListener("mouseup", (event) => {
    canvas.removeEventListener("mousemove", draw);
    endX = event.offsetX;
    endY = event.offsetY;
    switch(tool) {
        case 'brush':
            break;
        case 'filledRect':
            ctx.fillRect(startX, startY, endX-startX, endY-startY);
            break;
        case 'strokeRect':
            ctx.strokeRect(startX, startY, endX-startX, endY-startY);
            break;
        case 'circle':
            ctx.ellipse((startX+endX)/2, (startY+endY)/2, Math.abs((endX-startX)/2), Math.abs((endY-startY)/2), 0, 0, Math.PI*2);
            ctx.fill();
            break;
        case 'triangle':
            ctx.moveTo((startX+endX)/2, startY);
            ctx.lineTo(startX, endY);
            ctx.lineTo(endX, endY);
            ctx.fill();
            break;
        default:
            break;
    }
    picPush();
});

imgLoader.addEventListener("change", uploadImg);

function init(){
    ctx.fillStyle = "white";
    ctx.fillRect(0, 0, canvas.width, canvas.height);
    picPush();
    console.log(picStep);
    ctx.fillStyle = colorSelector.style.color;
    ctx.strokeStyle =  colorSelector.style.color;
    ctx.lineCap = 'round';
    ctx.lineJoin = 'round';
    $(".colorPickSelector").colorPick();
}

var lastTool;

function changeTool(obj) {
    if(lastTool != undefined)   lastTool.classList.remove("toolChoosen");
    tool = obj.id;
    obj.classList.add("toolChoosen");
    lastTool = obj;
    switch(tool) {
        case 'brush':
            canvas.style.cursor = "url('cursorBrush.png'), auto";
            break;
        case 'eraser':
            canvas.style.cursor = "url('cursorEraser.cur'), auto";
            break;
        default:
            canvas.style.cursor = "url('cursorCross.cur'), auto";
            break;

    }
}

function distanceBetween(point1, point2) {
    return Math.sqrt(Math.pow(point2.x - point1.x, 2) + Math.pow(point2.y - point1.y, 2));
}
function angleBetween(point1, point2) {
    return Math.atan2( point2.x - point1.x, point2.y - point1.y );
}
async function draw(event) {
    switch(tool){
        case 'rainbow':
            hue += 3;
            ctx.fillStyle = "hsl(" + hue + ", 100%, 50%)";
            ctx.strokeStyle = "hsl(" + hue + ", 100%, 50%)";
        case 'brush':
            var currentPoint = { x: event.offsetX, y: event.offsetY };
            var dist = distanceBetween(lastPoint, currentPoint);
            var angle = angleBetween(lastPoint, currentPoint);
            for (var i = 0; i < dist; i+=5) {
                var x = lastPoint.x + (Math.sin(angle) * i) - size;
                var y = lastPoint.y + (Math.cos(angle) * i) - size;
                ctx.beginPath();
                ctx.arc(x+size/2, y+size/2, size, false, Math.PI * 2, false);
                ctx.closePath();
                ctx.fill();
                ctx.stroke();
            }
            lastPoint = currentPoint;
            break;
        case 'eraser':
            ctx.lineTo(event.offsetX, event.offsetY);
            ctx.stroke();
            break;
        case 'filledRect':
            canvasPic.src = picArray[picStep];
            canvasPic.onload = function () {
                ctx.drawImage(canvasPic, 0, 0);
                ctx.fillRect(startX, startY, event.offsetX-startX, event.offsetY-startY);
            }
            break;
        case 'strokeRect':
            canvasPic.src = picArray[picStep];
            canvasPic.onload = function () {
                ctx.drawImage(canvasPic, 0, 0);
                ctx.strokeRect(startX, startY, event.offsetX-startX, event.offsetY-startY);
            }
            break;
        case 'triangle':
            canvasPic.src = picArray[picStep];
            canvasPic.onload = function () {
                ctx.drawImage(canvasPic, 0, 0);
                ctx.beginPath();
                ctx.moveTo((startX+event.offsetX)/2, startY);
                ctx.lineTo(startX, event.offsetY);
                ctx.lineTo(event.offsetX, event.offsetY);
                ctx.fill();
            }
            break;
        case 'circle':
            canvasPic.src = picArray[picStep];
            canvasPic.onload = function () {
                ctx.drawImage(canvasPic, 0, 0);
                ctx.beginPath();
                ctx.ellipse((startX+event.offsetX)/2, (startY+event.offsetY)/2, Math.abs((event.offsetX-startX)/2), Math.abs((event.offsetY-startY)/2), 0, 0, Math.PI*2);
                ctx.fill();
            }
            break;
        default:
            break;
    }
}

function typeText(){
    let text = document.createElement('input');
    document.getElementById('canvasDiv').appendChild(text);
    text.style.position = "absolute";
    text.style.border = "none";
    text.style.outline = "none";
    text.style.background = "none";
    text.style.fontFamily = font;
    text.style.fontSize = `${fontSize}px`;
    text.style.top = `${startY}px`;
    text.style.left = `${startX}px`;
}

function changeFont(y){
    font = y;
}

function changeFontSize(x){
    fontSize = x;
}

function picPush() {
    picStep++;
    if (picStep < picArray.length) { picArray.length = picStep; }
    picArray.slice(0, picStep);
    // picStep++;
    picArray.push(canvas.toDataURL());
}

function undo() {
    if (picStep > 0) {
        picStep--;
        canvasPic.src = picArray[picStep];
        canvasPic.onload = function () {
            ctx.drawImage(canvasPic, 0, 0);
        }
    }
}

function redo() {
    if (picStep < picArray.length-1) {
        picStep++;
        canvasPic.src = picArray[picStep];
        canvasPic.onload = function () {
            ctx.drawImage(canvasPic, 0, 0);
        }
    }
}

function changeColor(input) {
    color = input;
}

function changeSize(val) {
    size = val;
}

function saveImg() {
    let link = document.createElement('a');
    if(picSav == 0)  link.download = "sav-image.png";
    else    link.download = "sav-image-" + String(picSav) + ".png";
    link.href = picArray[picStep];
    link.click();
    picSav++;
}

async function uploadImg(e){
    let prm = new Promise((res, rej) => {
        var reader = new FileReader();
        reader.onload = function(event){
            var img = new Image();
            img.src = event.target.result;
            img.onload = function(){
                // canvas.width = img.width;
                // canvas.height = img.height;
                ctx.drawImage(img,0,0);
                res();
            }
        }
        reader.readAsDataURL(e.target.files[0]);

    });
    await prm;
    picPush();
    imgLoader.value = '';
}

$(".colorPickSelector").colorPick({
    'initialColor': '#c0392b',
    'allowRecent': true,
    'recentMax': 5,
    'palette': ["#1abc9c", "#16a085", "#2ecc71", "#27ae60", "#3498db", "#2980b9", "#9b59b6", "#8e44ad", "#34495e", "#2c3e50", "#f1c40f", "#f39c12", "#e67e22", "#d35400", "#e74c3c", "#c0392b", "#ecf0f1", "#bdc3c7", "#95a5a6", "#7f8c8d"],
    'onColorSelected': function() {
        this.element.css({'backgroundColor': this.color, 'color': this.color});
    }
});

