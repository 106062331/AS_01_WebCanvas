# Software Studio 2018 Spring Assignment 01 Web Canvas

## Web Canvas
<img src="example01.gif" width="700px" height="500px"></img>

## Todo
1. **Fork the repo ,remove fork relationship and change project visibility to public.**
2. Create your own web page with HTML5 canvas element where we can draw somethings.
3. Beautify appearance (CSS).
4. Design user interaction widgets and control tools for custom setting or editing (JavaScript).
5. **Commit to "your" project repository and deploy to Gitlab page.**
6. **Describing the functions of your canvas in REABME.md**

## Scoring (Check detailed requirments via iLMS)

| **Item**                                         | **Score** |
| :----------------------------------------------: | :-------: |
| Basic components                                 | 60%       |
| Advance tools                                    | 35%       |
| Appearance (subjective)                          | 5%        |
| Other useful widgets (**describe on README.md**) | 1~10%     |

## Reminder
* Do not make any change to our root project repository.
* Deploy your web page to Gitlab page, and ensure it works correctly.
* **Your main page should be named as ```index.html```**
* **URL should be : https://[studentID].gitlab.io/AS_01_WebCanvas**
* You should also upload all source code to iLMS.
* .html or .htm, .css, .js, etc.
* source files
* **Deadline: 2018/04/05 23:59 (commit time)**
* Delay will get 0 point (no reason)
* Copy will get 0 point
* "屍體" and 404 is not allowed

---

## Put your report below here

* **Basic**

每次按下不同的tool button，會改變tool（global variable）的值。
在canvas上mousedown時addEventListener，且在event中加入mousemove的eventListener，呼叫draw function。在mouseup的時候remove draw function。再依據tool的值來決定mousedown, mousemove, mouseup要做的事。

&#10003; Brush & Eraser : 
每次mousemove都會畫出一個實心圓，但畫圓的頻率太慢，會變成不連貫的圓點，因此要在兩個不連貫的點之間補上幾個圓。
[reference](http://perfectionkills.com/exploring-canvas-drawing-techniques/)

&#10003; Brush Size : 
改變range的value時（onmouseup event) 呼叫changeSize function。

&#10003; Color Selector : 
每次click時呼叫changeColor，並傳入this.style.color。
Use Css From [here](https://github.com/philzet/ColorPick.js)

&#10003; Text Input & Font Menu : 
在canvas mousedown時新增一個input tag在鼠標的位置。

&#10003; Change Cursor : 
在按下button時改變cursor style。

&#10003; Reset Canvas : 
在canvas上畫一個長寬等於canvas寬高的白色矩形。

* **Advanced**

&#10003; Different Brush Shape : 
在滑鼠還沒放開時就要畫出圖形，直到滑鼠放開再放到畫布上。
在每次mousemove時都先放上mousedown前存起來的canvas樣貌（canvas.toDataURL()），再畫出圖形。需要注意asynchronous的特性，因此把畫出圖形的部分寫在onload裡，避免畫出圖形之後image才load完成，把image覆蓋掉。

&#10003; Undo & Redo : 
在global放一個array -- picArray，每次mouseup都把canvas push進array裡，再用一個變數picStep紀錄現在要取array的哪格。

&#10003; Upload Image : 
使用`<input type="file">`

&#10003; Download Canvas : 
新增一個a element，再使用他的download函式。

* **Other useful widgets**

&#10003; Rainbow : 
每當筆刷新增一個圓都改變顏色。用hsl可以較輕鬆製作彩虹。


